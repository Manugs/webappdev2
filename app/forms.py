from flask_wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import DataRequired

#Format of the form where the user will write the information they want to upload
class CreateTaskForm(Form):
    title = TextField('title', validators=[DataRequired()])
    desc = TextAreaField('desc')